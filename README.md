# Description

A demo application for processing a tournaments. Written in Go using https://github.com/go-gem, http://jinzhu.me/gorm/ and https://github.com/gavv/httpexpect for integration testing.

# How to install

1. Install Docker-ce from the [**Docker official site**](https://www.docker.com/).
2. Install docker-compose.
3. Run "docker-compose up" in the root folder of the application.

The app is installed, tested and started!
Now you are able to send the commands for the server using address of Docker machine (or localhost if Docker installed locally) with the port 8088 (by default).

# Some notes about application config.

1. You can customize service PORT and DB params in .env file and docker-compose.yml.
2. You can select database engine by setting DB_DIALECT and DB_URI in docker-compose.yml from http://jinzhu.me/gorm/database.html#connecting-to-a-database.
Don't forget to change db image in docker-compose.yml (default is Postgres).

# Docker image

You can get it from https://hub.docker.com/r/ldim/score/