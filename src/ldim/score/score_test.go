package main

import (
	"fmt"
	"github.com/gavv/httpexpect"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
	"testing"
)

type Winner struct {
	PlayerId string `json:"playerId"`
	Prize    int    `json:"prize"`
}

type Winners struct {
	TournamentId string   `json:"tournamentId"`
	Winners      []Winner `json:"winners"`
}

var host = os.Getenv("TESTING_HOST")

func batchBalance(wg *sync.WaitGroup, e *httpexpect.Expect, op string, delta int, count int) {
	log.Print("Go ", op)
	for i := 0; i < count; i++ {
		e.GET(op).WithQuery("playerId", "PTEST").WithQuery("points", delta).Expect().Status(http.StatusOK)
	}
	wg.Done()
}

func TestBalance(t *testing.T) {
	concurrency, err := strconv.Atoi(os.Getenv("TEST_CONCURRENCY"))
	if err != nil {
		concurrency = 1
	}
	load, err := strconv.Atoi(os.Getenv("TEST_LOAD"))
	if err != nil {
		load = 1
	}
	e := httpexpect.New(t, host)
	e.GET("/reset").Expect().Status(http.StatusOK)
	balance := concurrency * 10 * load
	e.GET("/fund").WithQuery("playerId", "PTEST").WithQuery("points", balance).Expect().Status(http.StatusOK)
	var wg sync.WaitGroup
	wg.Add(concurrency * 2)
	for i := 0; i < concurrency; i++ {
		go batchBalance(&wg, e, "/take", 10, load)
	}
	for i := 0; i < concurrency; i++ {
		go batchBalance(&wg, e, "/fund", 10, load+10)
	}
	wg.Wait()
	e.GET("/balance").WithQuery("playerId", "PTEST").Expect().Status(http.StatusOK).JSON().Object().ValueNotEqual("balance", balance)
	e.GET("/balance").WithQuery("playerId", "PTEST").Expect().Status(http.StatusOK).JSON().Object().ValueEqual("balance", concurrency*10*(load+10))
}

func TestScore(t *testing.T) {
	e := httpexpect.New(t, host)

	e.GET("/reset").Expect().Status(http.StatusOK)

	e.GET("/fund").WithQuery("playerId", "P1").WithQuery("points", 600).Expect().Status(http.StatusOK)
	e.GET("/fund").WithQuery("playerId", "P1").WithQuery("points", "pts").Expect().Status(http.StatusAccepted)
	e.GET("/take").WithQuery("playerId", "P1").WithQuery("points", 300).Expect().Status(http.StatusOK)
	e.GET("/take").WithQuery("playerId", "P1").WithQuery("points", -1).Expect().Status(http.StatusAccepted)
	e.GET("/fund").WithQuery("playerId", "P1").WithQuery("points", -1).Expect().Status(http.StatusAccepted)
	e.GET("/take").WithQuery("playerId", "P1").WithQuery("points", 600).Expect().Status(http.StatusAccepted)
	e.GET("/fund").WithQuery("playerId", "P2").WithQuery("points", 300).Expect().Status(http.StatusOK)
	e.GET("/fund").WithQuery("playerId", "P3").WithQuery("points", 300).Expect().Status(http.StatusOK)
	e.GET("/fund").WithQuery("playerId", "P4").WithQuery("points", 300).Expect().Status(http.StatusOK)
	e.GET("/fund").WithQuery("playerId", "P4").WithQuery("points", 200).Expect().Status(http.StatusOK)
	e.GET("/fund").WithQuery("playerId", "P5").WithQuery("points", 2000).Expect().Status(http.StatusOK)

	e.GET("/announceTournament").WithQuery("tournamentId", "T1").WithQuery("deposit", -1000).Expect().Status(http.StatusAccepted)
	e.GET("/announceTournament").WithQuery("tournamentId", "T1").WithQuery("deposit", 1000).Expect().Status(http.StatusOK)

	e.GET("/joinTournament").WithQuery("tournamentId", "T1").WithQuery("playerId", "P5").Expect().Status(http.StatusOK)
	e.GET("/joinTournament").WithQuery("tournamentId", "T1").WithQuery("playerId", "P5").Expect().Status(http.StatusAccepted)
	e.GET("/joinTournament").WithQuery("tournamentId", "T1").WithQuery("playerId", "P1").
		WithQuery("backerId", "P2").WithQuery("backerId", "P3").WithQuery("backerId", "P4").
		Expect().Status(http.StatusOK)

	e.POST("/resultTournament").WithJSON(Winners{"T1", []Winner{{"P1", 2000}}}).Expect().Status(http.StatusOK)
	e.POST("/resultTournament").WithJSON(Winners{"T1", []Winner{{"P1", 2000}}}).Expect().Status(http.StatusAccepted)

	e.GET("/balance").WithQuery("playerId", "P1").Expect().Status(http.StatusOK).JSON().Object().ValueEqual("balance", 550)
	e.GET("/balance").WithQuery("playerId", "P2").Expect().Status(http.StatusOK).JSON().Object().ValueEqual("balance", 550)
	e.GET("/balance").WithQuery("playerId", "P3").Expect().Status(http.StatusOK).JSON().Object().ValueEqual("balance", 550)
	e.GET("/balance").WithQuery("playerId", "P4").Expect().Status(http.StatusOK).JSON().Object().ValueEqual("balance", 750)
	e.GET("/balance").WithQuery("playerId", "P5").Expect().Status(http.StatusOK).JSON().Object().ValueEqual("balance", 1000)
}

func TestDeadlock(t *testing.T) {
	e := httpexpect.New(t, host)

	e.GET("/reset").Expect().Status(http.StatusOK)

	e.GET("/fund").WithQuery("playerId", "P1").WithQuery("points", 10000).Expect().Status(http.StatusOK)
	e.GET("/fund").WithQuery("playerId", "P2").WithQuery("points", 10000).Expect().Status(http.StatusOK)
	e.GET("/fund").WithQuery("playerId", "P3").WithQuery("points", 10000).Expect().Status(http.StatusOK)
	e.GET("/fund").WithQuery("playerId", "P4").WithQuery("points", 10000).Expect().Status(http.StatusOK)

	tc := 10
	for i := 0; i < tc; i++ {
		e.GET("/announceTournament").WithQuery("tournamentId", fmt.Sprintf("T%d", i)).WithQuery("deposit", 40).Expect().Status(http.StatusOK)
	}

	var wg sync.WaitGroup
	wg.Add(4)
	go func() {
		for i := 0; i < tc; i++ {
			e.GET("/joinTournament").WithQuery("tournamentId", fmt.Sprintf("T%d", i)).WithQuery("playerId", "P1").
				WithQuery("backerId", "P2").WithQuery("backerId", "P3").WithQuery("backerId", "P4").
				Expect().Status(http.StatusOK)
		}
		wg.Done()
	}()
	go func() {
		for i := 0; i < tc; i++ {
			e.GET("/joinTournament").WithQuery("tournamentId", fmt.Sprintf("T%d", i)).WithQuery("playerId", "P2").
				WithQuery("backerId", "P1").WithQuery("backerId", "P3").WithQuery("backerId", "P4").
				Expect().Status(http.StatusOK)
		}
		wg.Done()
	}()
	go func() {
		for i := 0; i < tc; i++ {
			e.GET("/joinTournament").WithQuery("tournamentId", fmt.Sprintf("T%d", i)).WithQuery("playerId", "P3").
				WithQuery("backerId", "P1").WithQuery("backerId", "P2").WithQuery("backerId", "P4").
				Expect().Status(http.StatusOK)
		}
		wg.Done()
	}()
	go func() {
		for i := 0; i < tc; i++ {
			e.GET("/joinTournament").WithQuery("tournamentId", fmt.Sprintf("T%d", i)).WithQuery("playerId", "P4").
				WithQuery("backerId", "P1").WithQuery("backerId", "P2").WithQuery("backerId", "P3").
				Expect().Status(http.StatusOK)
		}
		wg.Done()
	}()
	wg.Wait()
	wg.Add(2)
	go func() {
		for i := 0; i < tc/2; i++ {
			e.POST("/resultTournament").WithJSON(Winners{fmt.Sprintf("T%d", i), []Winner{{"P1", 2000}}}).Expect().Status(http.StatusOK)
		}
		wg.Done()
	}()
	go func() {
		for i := tc / 2; i < tc; i++ {
			e.POST("/resultTournament").WithJSON(Winners{fmt.Sprintf("T%d", i), []Winner{{"P2", 2000}}}).Expect().Status(http.StatusOK)
		}
		wg.Done()
	}()
	wg.Wait()
}
