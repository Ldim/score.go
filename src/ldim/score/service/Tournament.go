package service

import (
	"errors"
	"github.com/go-gem/log"

	"ldim/score/dto"
	"ldim/score/model"
	"ldim/score/repository"
	"sort"
)

type TTournament func(id string) Tournament

type Tournament interface {
	Deposit(int) error
	Join(string, []string) error
	Result([]dto.Winner) error
	Reset() error
}

type tournamentImpl struct {
	db repository.Tournament
	id string
}

func NewTournament(id string) Tournament {
	return &tournamentImpl{repository.NewTournament(), id}
}

func (t *tournamentImpl) Deposit(amount int) error {
	log.Debug("announce tournament")
	tournament := model.Tournament{TournamentId: t.id, Deposit: amount, Status: "active"}
	if err := t.db.SaveTournament(&tournament); err != nil {
		t.db.Rollback()
		return err
	}
	t.db.Commit()
	return nil
}

func (t *tournamentImpl) Join(playerId string, backerIds []string) error {
	log.Debug("join tournament")
	tournament, err := t.db.FindTournament(t.id)
	if err != nil {
		t.db.Rollback()
		return err
	}
	if tournament.Status == "finished" {
		t.db.Rollback()
		return errors.New("Tournament is finished")
	}
	if tournament.Status == "active" {
		tournament.Status = "started"
		if err := t.db.UpdateTournament(tournament); err != nil {
			t.db.Rollback()
			return err
		}
	}
	ids := []string{playerId}
	ids = append(ids, backerIds...)
	sort.Strings(ids)
	amount := tournament.Deposit / len(ids)

	for _, id := range ids {
		if err := t.playerBid(id, amount, playerId); err != nil {
			t.db.Rollback()
			return err
		}
	}
	t.db.Commit()
	return nil
}

func (t *tournamentImpl) playerBid(playerId string, amount int, bakerId string) error {
	if err := t.db.UpdatePlayerBalance(playerId, -amount); err != nil {
		return err
	}
	return t.db.SaveBid(&model.Bid{TournamentId: t.id, PlayerId: playerId, Amount: amount, BackedId: bakerId})
}

func (t *tournamentImpl) Result(winners []dto.Winner) error {
	log.Debug("tournament result")
	tournament, err := t.db.FindTournament(t.id)
	if err != nil {
		t.db.Rollback()
		return err
	}
	if err := t.db.ChangeTournamentStatus(tournament, "started", "finished"); err != nil {
		t.db.Rollback()
		return err
	}
	for _, winner := range winners {
		if bids, err := t.db.FindBidsByTournamentPlayer(t.id, winner.PlayerId); err == nil {
			players := len(*bids)
			if players == 0 {
				t.db.Commit()
				return errors.New("No players for tournament")
			}
			prize := winner.Prize / players
			var bds = append([]model.Bid{}, *bids...)
			sort.Slice(bds, func(i, j int) bool { return bds[i].PlayerId < bds[j].PlayerId })
			for _, bid := range bds {
				if err := t.db.UpdatePlayerBalance(bid.PlayerId, prize); err != nil {
					t.db.Rollback()
					return err
				}
			}
		} else {
			t.db.Rollback()
			return err
		}
	}
	t.db.Commit()
	return nil
}

func (t *tournamentImpl) Reset() error {
	log.Debug("reset")
	t.db.DeleteAllTournaments()
	t.db.DeleteAllPlayers()
	t.db.DeleteAllBids()
	t.db.Commit()
	return nil
}
