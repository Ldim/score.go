package service

import (
	"github.com/go-gem/log"
	"ldim/score/dto"
	"ldim/score/model"
	"ldim/score/repository"
)

type TPlayer func(id string) Player

type Player interface {
	Fund(int) error
	Take(int) error
	Balance() (dto.Player, error)
}

type playerImpl struct {
	db repository.Player
	id string
}

func NewPlayer(id string) Player {
	return &playerImpl{repository.NewPlayer(), id}
}

func (p *playerImpl) Fund(points int) error {
	log.Debug("Fund")
	player := model.Player{PlayerId: p.id, Balance: points}
	if err := p.db.SavePlayer(&player); err != nil {
		p.db.Rollback()
		return err
	}
	p.db.Commit()
	return nil
}

func (p *playerImpl) Take(points int) error {
	log.Debug("Take")
	if err := p.db.UpdatePlayerBalance(p.id, -points); err != nil {
		p.db.Rollback()
		return err
	}
	p.db.Commit()
	return nil
}

func (p *playerImpl) Balance() (dto.Player, error) {
	log.Debug("Balance")
	defer p.db.Commit()
	player, err := p.db.FindPlayer(p.id)
	if err != nil {
		return dto.Player{}, err
	}
	return dto.Player{player.PlayerId, player.Balance}, nil
}
