package model

import "github.com/jinzhu/gorm"

type TTournament interface {
	FindTournament(string) (*Tournament, error)
	SaveTournament(*Tournament) error
	UpdateTournament(*Tournament) error
	ChangeTournamentStatus(*Tournament, string, string) error
	DeleteAllTournaments() error
}

type Tournament struct {
	gorm.Model
	TournamentId string `gorm:"unique_index:idx_unique_tournament"`
	Deposit      int
	Status       string
}
