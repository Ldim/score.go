package model

import "github.com/jinzhu/gorm"

type TBid interface {
	FindBidsByTournamentPlayer(string, string) (*[]Bid, error)
	SaveBid(*Bid) error
	DeleteAllBids() error
}

type Bid struct {
	gorm.Model
	TournamentId string `gorm:"unique_index:idx_unique_bid"`
	PlayerId     string `gorm:"unique_index:idx_unique_bid"`
	Amount       int
	BackedId     string `gorm:"unique_index:idx_unique_bid"`
}
