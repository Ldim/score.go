package model

import "github.com/jinzhu/gorm"

type TPlayer interface {
	FindPlayer(string) (*Player, error)
	SavePlayer(*Player) error
	UpdatePlayerBalance(playerId string, delta int) error
	DeleteAllPlayers() error
}

type Player struct {
	gorm.Model
	PlayerId string `gorm:"unique_index:idx_unique_player"`
	Balance  int
}
