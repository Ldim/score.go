package dto

import "encoding/json"

type Winner struct {
	PlayerIdRaw json.RawMessage `json:"playerId"`
	PlayerId    string
	Prize       int `json:"prize"`
}

type winner Winner

func (w *Winner) UnmarshalJSON(b []byte) (err error) {
	o := winner{}
	if err = json.Unmarshal(b, &o); err != nil {
		return
	}
	o.PlayerId, err = convertRawIdToString(o.PlayerIdRaw)
	*w = Winner(o)
	return
}
