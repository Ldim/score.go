package dto

type Player struct {
	PlayerId string `json:"playerId"`
	Balance int `json:"balance"`
}
