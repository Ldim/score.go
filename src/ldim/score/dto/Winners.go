package dto

import (
	"encoding/json"
	"io"
)

type Winners struct {
	TournamentIdRaw json.RawMessage `json:"tournamentId"`
	TournamentId    string
	Winners         []Winner `json:"winners"`
}

type winners Winners

func (ws *Winners) UnmarshalJSON(b []byte) (err error) {
	o := winners{}
	if err = json.Unmarshal(b, &o); err != nil {
		return
	}
	o.TournamentId, err = convertRawIdToString(o.TournamentIdRaw)
	*ws = Winners(o)
	return
}

func DecodeWinners(r io.Reader) (x *Winners, err error) {
	x = new(Winners)
	err = json.NewDecoder(r).Decode(x)
	return
}
