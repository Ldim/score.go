package dto

import (
	"encoding/json"
	"fmt"
)

func convertRawIdToString(data []byte) (s string, err error) {
	s, n := "", uint64(0)
	if err = json.Unmarshal(data, &s); err == nil {
		return
	}
	if err = json.Unmarshal(data, &n); err == nil {
		s = fmt.Sprintf("%d", n)
	}
	return
}
