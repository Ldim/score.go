package dto

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestIntId(t *testing.T) {
	data := []byte("{\"tournamentId\": 1, \"winners\": [{\"playerId\": 1, \"prize\": 500}]}")
	var winners Winners
	json.Unmarshal(data, &winners)
	assertNotEqual(t, winners.TournamentId, 1)
	assertEqual(t, winners.TournamentId, "1")
	assertEqual(t, winners.Winners[0].PlayerId, "1")
}

func TestStrId(t *testing.T) {
	data := []byte("{\"tournamentId\": \"T1\", \"winners\": [{\"playerId\": \"P1\", \"prize\": 500}]}")
	var winners Winners
	json.Unmarshal(data, &winners)
	assertEqual(t, winners.TournamentId, "T1")
	assertEqual(t, winners.Winners[0].PlayerId, "P1")
}

func assertEqual(t *testing.T, a interface{}, b interface{}, message ...string) {
	if a == b {
		return
	}
	if len(message) == 0 {
		t.Fatal(fmt.Sprintf("%v != %v", a, b))
	}
	t.Fatal(message[0])
}

func assertNotEqual(t *testing.T, a interface{}, b interface{}, message ...string) {
	if a != b {
		return
	}
	if len(message) == 0 {
		t.Fatal(fmt.Sprintf("%v == %v", a, b))
	}
	t.Fatal(message[0])
}
