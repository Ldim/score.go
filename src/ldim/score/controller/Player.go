package controller

import (
	"github.com/go-gem/gem"
	"ldim/score/service"
)

type Player struct {
	Player service.TPlayer
}

func (p *Player) Fund(ctx *gem.Context) {
	playerId := ctx.URL().Query().Get("playerId")
	points, err := validateIntParam(ctx, "points")
	if err != nil {
		ctx.JSON(202, Error{err.Error()})
		return
	}
	if err := p.Player(playerId).Fund(points); err != nil {
		ctx.JSON(202, Error{err.Error()})
	}
}

func (p *Player) Take(ctx *gem.Context) {
	playerId := ctx.URL().Query().Get("playerId")
	points, err := validateIntParam(ctx, "points")
	if err != nil {
		ctx.JSON(202, Error{err.Error()})
		return
	}
	if err := p.Player(playerId).Take(points); err != nil {
		ctx.JSON(202, Error{err.Error()})
	}
}

func (p *Player) Balance(ctx *gem.Context) {
	playerId := ctx.URL().Query().Get("playerId")
	if player, err := p.Player(playerId).Balance(); err == nil {
		ctx.JSON(200, player)
	} else {
		ctx.JSON(202, Error{err.Error()})
	}
}