package controller

import (
	"github.com/go-gem/gem"
	"ldim/score/dto"
	"ldim/score/service"
)

type Tournament struct {
	Tournament service.TTournament
}

func (t *Tournament) Announce(ctx *gem.Context) {
	tournamentId := ctx.URL().Query().Get("tournamentId")
	deposit, err := validateIntParam(ctx, "deposit")
	if err != nil {
		ctx.JSON(202, Error{err.Error()})
		return
	}
	if err := t.Tournament(tournamentId).Deposit(deposit); err != nil {
		ctx.JSON(202, Error{err.Error()})
	}
}

func (t *Tournament) Join(ctx *gem.Context) {
	tournamentId := ctx.URL().Query().Get("tournamentId")
	playerId := ctx.URL().Query().Get("playerId")
	backerIds := ctx.URL().Query()["backerId"]
	if err := t.Tournament(tournamentId).Join(playerId, backerIds); err != nil {
		ctx.JSON(202, Error{err.Error()})
	}
}

func (t *Tournament) Result(ctx *gem.Context) {
	winners, err := dto.DecodeWinners(ctx.Request.Body)
	if err != nil {
		ctx.JSON(202, Error{err.Error()})
		return
	}
	if err := t.Tournament(winners.TournamentId).Result(winners.Winners); err != nil {
		ctx.JSON(202, Error{err.Error()})
	}
}

func (t *Tournament) Reset(ctx *gem.Context) {
	if err := t.Tournament("").Reset(); err != nil {
		ctx.JSON(202, Error{err.Error()})
	}
}
