package controller

const (
	FUND_URL = "/fund"
	TAKE_URL = "/take"
	BALANCE_URL = "/balance"
	ANNOUNCE_URL = "/announceTournament"
	JOIN_URL = "/joinTournament"
	RESULT_URL = "/resultTournament"
	RESET_URL = "/reset"
	HEALTH_URL = "/health"
)
