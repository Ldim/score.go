package controller

import (
	"fmt"
	"github.com/go-gem/gem"
	"github.com/go-gem/log"
	"strconv"
)

func validateIntParam(ctx *gem.Context, name string) (int, error) {
	val, err := strconv.Atoi(ctx.URL().Query().Get(name))
	if err != nil {
		err = fmt.Errorf("Integer param [%s] validation: %v", name, err)
		log.Debug(err)
		return 0, err
	}
	if val < 0 {
		err = fmt.Errorf("Integer param [%s] must be positive", name)
		log.Debug(err)
		return 0, err
	}
	return val, nil

}
