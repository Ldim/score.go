package repository

import (
	"fmt"
	"github.com/go-gem/log"
	"ldim/score/model"
)

func (db *db) FindBidsByTournamentPlayer(tournamentId, backedId string) (*[]model.Bid, error) {
	var bids []model.Bid
	err := db.Find(&bids, "tournament_id = ? and backed_id = ?", tournamentId, backedId).Error
	if err != nil {
		err = fmt.Errorf("Find bids by[%s,%s]: %v", tournamentId, backedId, err)
		log.Debug(err)
		return &bids, err
	}
	return &bids, nil
}

func (db *db) SaveBid(bid *model.Bid) error {
	err := db.Create(bid).Error
	if err != nil {
		err = fmt.Errorf("Save bid[%s,%s,%d,%s]: %v", bid.TournamentId, bid.PlayerId, bid.Amount, bid.BackedId, err)
		log.Debug(err)
		return err
	}
	return nil
}

func (db *db) DeleteAllBids() error {
	err := db.exclusiveDelete("bids")
	if err != nil {
		err = fmt.Errorf("Delete all bids: %v", err)
		log.Debug(err)
		return err
	}
	return nil
}
