package repository

import (
	"fmt"
	"github.com/go-gem/log"
	"github.com/jinzhu/gorm"
	"ldim/score/model"
)

func (db *db) FindPlayer(id string) (*model.Player, error) {
	var player model.Player
	err := db.Find(&player, "player_id = ?", id).Error
	if err != nil {
		err = fmt.Errorf("Find player %s: %v", id, err)
		log.Debug(err)
		return &player, err
	}
	return &player, nil
}

func (db *db) SavePlayer(player *model.Player) error {
	err := db.Set("gorm:query_option", "FOR UPDATE").
		Assign("balance", gorm.Expr("balance + ?", player.Balance)).
		FirstOrCreate(&player, "player_id = ?", player.PlayerId).Error
	if err != nil {
		err = fmt.Errorf("Save player %s: %s", player.PlayerId, err)
		log.Debug(err)
		return err
	}
	return nil
}

func (db *db) UpdatePlayerBalance(playerId string, delta int) error {
	player := model.Player{}
	if err := db.Set("gorm:query_option", "FOR UPDATE").
		First(&player, "player_id = ?", playerId).Error; err != nil {
		return err
	}
	if player.Balance+delta < 0 {
		err := fmt.Errorf("Can't update player %s balance by %d: less than zero", player.PlayerId, delta)
		log.Debug(err)
		return err
	}
	player.Balance += delta
	if err := db.Save(&player).Error; err != nil {
		err := fmt.Errorf("Update player %s balance by %d: %s", player.PlayerId, delta, err)
		log.Debug(err)
		return err
	}
	return nil
}

func (db *db) DeleteAllPlayers() error {
	err := db.exclusiveDelete("players")
	if err != nil {
		err = fmt.Errorf("Delete all players: %v", err)
		log.Debug(err)
		return err
	}
	return nil
}
