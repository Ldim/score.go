package repository

import "ldim/score/model"

type Player interface {
	tx
	model.TPlayer
}

func NewPlayer() Player {
	return &db{connect().Begin()}
}
