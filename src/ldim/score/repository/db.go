package repository

import (
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type tx interface {
	Commit() *gorm.DB
	Rollback() *gorm.DB
}

type db struct {
	*gorm.DB
}

var conn *gorm.DB

func connect() *gorm.DB {
	if conn == nil {
		var err error
		conn, err = gorm.Open(os.Getenv("DB_DIALECT"), os.Getenv("DB_URI"))
		if err != nil {
			panic(err)
		}
		//conn.LogMode(true)
	}
	return conn
}

func NewDB() *gorm.DB {
	return connect()
}
