package repository

import (
	"fmt"
	"github.com/go-gem/log"
	"ldim/score/model"
)

func (db *db) FindTournament(id string) (*model.Tournament, error) {
	var tournament model.Tournament
	err := db.Find(&tournament, "tournament_id = ?", id).Error
	if err != nil {
		err = fmt.Errorf("Find tournament %s: %v", id, err)
		log.Debug(err)
		return &tournament, err
	}
	return &tournament, nil
}

func (db *db) SaveTournament(t *model.Tournament) error {
	err := db.Set("gorm:query_option", "FOR UPDATE").
		Where("status = ?", t.Status).
		Assign("deposit", t.Deposit).
		FirstOrCreate(&t, "tournament_id = ?", t.TournamentId).Error
	if err != nil {
		err = fmt.Errorf("Save tournament %v: %v", t.TournamentId, err)
		log.Debug(err)
		return err
	}
	return nil
}

func (db *db) UpdateTournament(tournament *model.Tournament) error {
	err := db.Save(tournament).Error
	if err != nil {
		err = fmt.Errorf("Update tournament %v: %v", tournament.TournamentId, err)
		log.Debug(err)
		return err
	}
	return nil
}

func (db *db) ChangeTournamentStatus(tournament *model.Tournament, from, to string) error {
	res := db.Model(&tournament).Where("status = ?", from).UpdateColumn("status", to)
	if res.Error != nil {
		err := fmt.Errorf("Change tournament %s status: %s", tournament.TournamentId, res.Error)
		log.Debug(err)
		return err
	}
	if res.RowsAffected == 0 {
		err := fmt.Errorf("Tournament already %s", to)
		log.Debug(err)
		return err
	}
	return nil
}

func (db *db) DeleteAllTournaments() error {
	err := db.exclusiveDelete("tournaments")
	if err != nil {
		err = fmt.Errorf("Delete all tournaments: %v", err)
		log.Debug(err)
		return err
	}
	return nil
}
