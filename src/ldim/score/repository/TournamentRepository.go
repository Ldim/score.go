package repository

import "ldim/score/model"

type Tournament interface {
	tx
	model.TBid
	model.TPlayer
	model.TTournament
}

func NewTournament() Tournament {
	return &db{connect().Begin()}
}
