package repository

import (
	"github.com/go-gem/log"
	"ldim/score/model"
)

func Migrate() {
	log.Info("Migrate..")

	db := NewDB()

	db.AutoMigrate(&model.Player{}, &model.Tournament{}, &model.Bid{})

	db.Exec("ALTER TABLE public.players DROP CONSTRAINT IF EXISTS no_neg_balance;")
	db.Exec("ALTER TABLE public.players ADD CONSTRAINT no_neg_balance CHECK (balance >= 0);")

	log.Info("Done")
}
