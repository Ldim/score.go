package repository

import (
	"fmt"
	"github.com/go-gem/log"
	"time"
)

const (
	deleteAttempts        = 100
	deleteAttemptsTimeout = 10
)

func (db *db) exclusiveDelete(table string) error {
	var err error
	for i := 0; i < deleteAttempts; i++ {
		err = db.Exec(fmt.Sprintf("LOCK TABLE %s IN ACCESS EXCLUSIVE MODE NOWAIT", table)).Error
		if err == nil {
			err := db.Exec(fmt.Sprintf("DELETE FROM %s", table)).Error
			if err != nil {
				return err
			}
			return nil
		} else {
			log.Debug("Delete attempt ", i)
			time.Sleep(time.Millisecond * deleteAttemptsTimeout)
		}
	}
	return err
}
