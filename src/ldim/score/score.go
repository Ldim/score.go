package main

import (
	"log"

	"github.com/go-gem/gem"

	"ldim/score/controller"
	"ldim/score/service"
	"os"
	"ldim/score/repository"
)

var (
	player = controller.Player{service.NewPlayer}
	tournament = controller.Tournament{service.NewTournament}
)

type health struct {
	Alive bool `json:"alive"`
}

func main() {
	repository.Migrate()

	srv := gem.New(":" + os.Getenv("PORT"))

	router := gem.NewRouter()

	router.GET(controller.FUND_URL, player.Fund)

	router.GET(controller.TAKE_URL, player.Take)

	router.GET(controller.BALANCE_URL, player.Balance)

	router.GET(controller.ANNOUNCE_URL, tournament.Announce)

	router.GET(controller.JOIN_URL, tournament.Join)

	router.POST(controller.RESULT_URL, tournament.Result)

	router.GET(controller.RESET_URL, tournament.Reset)

	router.GET(controller.HEALTH_URL, func(ctx *gem.Context) {
		ctx.JSON(200, health{true})
	})

	log.Println(srv.ListenAndServe(router.Handler()))
}