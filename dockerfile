# build stage
FROM golang:alpine as builder

RUN apk update && apk add git && rm -rf /var/cache/apk/*

WORKDIR /go/src/ldim/score
COPY ./src/ldim/score .

RUN go-wrapper download
RUN go-wrapper install

# final stage
FROM alpine:latest

RUN apk update && apk add curl && rm -rf /var/cache/apk/*

WORKDIR /app
COPY --from=builder /go/bin/score .

ENTRYPOINT ["./score"]